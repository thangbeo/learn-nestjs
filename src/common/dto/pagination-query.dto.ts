import { Type } from 'class-transformer';
import { IsOptional, IsPositive } from 'class-validator';

export class PaginationQueryDto {
  @IsOptional() // meaning that no errors will be thrown if missing or undefined
  @IsPositive() // Nó kiểm tra xem giá trị của thuộc tính có phải là một số dương hay không
  @Type(() => Number)
  limit: number;

  @IsOptional()
  @IsPositive()
  @Type(() => Number)
  offset: number;
}
